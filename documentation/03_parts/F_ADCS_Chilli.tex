\chapter{Lageregelung}

Die Lageregelung ist zuständig für die korrekte Positionierung des Satelliten in der Erdumlaufbahn. Hierbei unterscheiden wir zwischen der Spinstabilisierung sowie der Lagestabilisierung. Wobei man bei der Lagestabilisierung zusätzlich noch zwischen der passiven und der aktiven Regelung unterscheidet. Natürlich gibt es noch andere Möglichkeiten für die korrekte Positionierung des Satelliten, aber zum groben Verständnis werden nur die gängigsten Methoden erläutert.

\section{Spinstabilisierung}
Die Spinstabilisierung ist definiert durch die Rotation um die Hauptträgheitsachse. In diesem Fall dient sie, wie schon aus dem Namen ersichtlich, zur Stabilisierung und zur Lageregelung des CubeSats. Der Satellit kann nicht aus seiner Lage im Raum bzw. im Schwerefeld eines Himmelskörpers gebracht werden, weil hierbei ein Effekt ausgenutzt wird, in dem der Satellit um seine Längsachse rotiert und sich dementsprechend wie ein Kreisel verhält. Dies ist ein wesentlicher Nachteil der Spinstabilisierung, da der Satellit nur mehr durch Steuerungstriebwerke im Raum ausgerichtet werden kann. Er ist sozusagen unempfindlich gegen äußeren Störeinflüssen.
\\
\\
Somit werden die Solarzellen am Umfang des Satelliten während der Rotation um die Hauptträgheitsachse nacheinander von der Sonne beschienen.
~\cite{bib:Stabilisierung}

\section{Lagestabilisierung}
Die Lagestabiliserung kann durch Sensoren, das Gravitations- oder Magnetfeld der Erde oder Drallräder erreicht werden. Für die Lagestabilisierung von CubeSats wird hauptsächlich das Erdmagnetfeld genutzt, da dies am wenigsten Energie und Platz benötigt.

\subsection{Passive Lagestabilisierung}
Bei der passiven Lageregelung nimmt der Satellit immer dieselbe Position entlang des Erdmagnetfeldes ein. Dies wird unter anderem mit Dauermagneten realisiert, welche mit dem Erdmagnetfeld eine konstante Kraftwirkung auf den Satelliten erzeugen.

\subsection{Aktive Lagestabilisierung}
Für die aktive Lageregelung gibt es mehrere Methoden, wobei die sogenannte Magnettorquer-Regelung auf häufigsten verwendet wird und auch für Oxomo geplant ist. Die Methode ist ähnlich wie bei der passiven Lageregelung, nur dass anstatt der Magneten, Spulen verwendet werden. Bei dieser Art der Lageregelung wird mittels einer Magnetspule ein magnetischer Dipol erzeugt. Das Magnetfeld der Spule interagiert mit dem Magnetfeld der Erde, womit dann ein Drehmoment auf den Satelliten wirkt.
\\
Die Intensität des Momentes lässt sich mit dem Strom, welcher durch die Spule fließt steuern. Das bedeutet, das Magnetfeld der Spule ist veränderbar und man kann somit die Position des Satelliten aktiv beeinflussen.
\\
Bei dieser Methode der Lagestabilisierung gibt es jedoch zwei Nachteile. Zum einen verringert sich der Wirkungsgrad mit größerem Abstand des Satelliten zur Erde. Der Grund hierfür ist der, dass das Erdmagnetfeld mit steigender Höhe abnimmt. Auf dies wird später noch genauer eingegangen.
\\
Ein weiterer Nachteil ist, dass das auf dem Satelliten wirkende Drehmoment relativ klein ist, weswegen eine Rotation eine gewisse Zeitdauer benötigt.
\\
Ein Vorteil dieser Stabilisierungsmethode ist, dass keine fossilen Treibstoffe benötigt werden und man allein mit elektrischer Energie operieren kann.
~\cite{bib:Drallrad}

\vspace{0.5cm}
\noindent\begin{minipage}{\linewidth}
	\centering
	\captionsetup{type=figure}
	\includegraphics[height=0.7\textwidth]{05_media/01_images/F_ADCS/interaction_of_magentic_fields}
	\captionof{figure}[Interaktion der Magnetfelder]{Interaktion der Magnetfelder}
	\label{fig:Interaktion der Magnetfelder}
\end{minipage}
\vspace{0.3cm}

\clearpage
\section{Auslegung der Spulen}
Die gesamte Berechnung erfolgte mithilfe von Formeln aus dem Internet sowie selbst hergeleiteten Formeln, beziehungsweise erlernten Fähigkeiten in der Ausbildung.

\subsection{Erdmagnetfeld}
Folgende Angaben sind für die Berechnung des Erdmagnetfeldes nötig:
\begin{equation} \label{eq:Erdradius}
r = 6.378.155m
\end{equation}

Der Erdradius $r$ aus Gleichung~\ref{eq:Erdradius} wurde aus einem geodätischen System (World Geodetic System 1984 – WGS84), das als einheitliche Grundlage für Positionsangaben auf der Erde dient, entnommen.
~\cite{bib:Erdradius}

\begin{equation} \label{eq:Dipolmoment}
M(year) = (7,746*10^{24} - (0.006*(year-2010)))nT*m^3
\end{equation}
\begin{equation}
M(2017) = (7,746*10^{24} - (0.006*(2017-2010)))nT*m^3
\end{equation}
\begin{equation}
M(2017) = 7,746*10^{24}*nT*m^3
\end{equation}
Unter dem magnetischen Dipol versteht man die Austrittsflächen des magnetischen Feldes aus einem Körper und somit ist das Dipolmoment $M$ die gesamte von einem magnetischen Dipol erzeugte Feldmenge.
\\
Das Dipolmoment $M$ in Gleichung~\ref{eq:Dipolmoment} Betrug im Jahr 2010:
\begin{equation}
M(2010) = 7.746*10^{24}nT*m^3
\end{equation}
Wobei die jährliche (zeitliche) Veränderung (Säkularvariation) zurzeit
\begin{equation} \label{eq:Säkularvariation}
-0.006*10^{24}nT*m^3*a^{-1}
\end{equation}
beträgt.~\cite{bib:Erdmagnetfeld}
\\
\\
Deshalb wurde das Dipolmoment mit dieser jährlichen Veränderung auf den jetzigen Stand berechnet. Hier ist auch sehr gut ersichtlich, dass die Änderungen sehr langsam erfolgen.

\clearpage
\begin{equation}\label{eq:Dipolfeld}
B(year,r,\lambda)=\dfrac{M(year)}{r^3}*\sqrt{1+3*(sin(\lambda))^2}
\end{equation}
Die geographische Breite $\lambda$ ist die nördliche oder südliche Entfernung eines Punktes auf der Erdoberfläche vom Äquator. Sie wird üblicherweise in Grad angegeben. Die geographische Breite erweist Werte von $0^{\circ}$ bis $\pm90^{\circ}$ am Nord bzw. Südpol. Daraus erkennt man, dass die Erde in 180 Breitenkreise aufgeteilt wird, die alle parallel zum Äquator verlaufen.~\cite{bib:Geographische_Breite}
\\
\\
Mit der Gleichung~\ref{eq:Dipolfeld} wurde dann das gesamte Erdmagnetfeld in Abhängigkeit der geographischen bzw. magnetischen Breite berechnet und der Verlauf in einem Diagramm \ref{fig:Erdmagnetfeld - Verlauf} dargestellt.

\vspace{0.5cm}
\noindent\begin{minipage}{\linewidth}
	\centering
	\captionsetup{type=figure}
	\fbox{\includegraphics[height=0.7\textwidth]{05_media/01_images/F_ADCS/erdmagnetfeld_verlauf}}
	\captionof{figure}[Erdmagnetfeld - Verlauf]{Erdmagnetfeld - Verlauf}
	\label{fig:Erdmagnetfeld - Verlauf}
\end{minipage}
\vspace{0.3cm}

Im Diagramm \ref{fig:Erdmagnetfeld - Verlauf} kann man sehr gut erkennen, dass das Erdmagnetfeld in Richtung der Pole immer größer wird.

\clearpage
\begin{equation}\label{eq:Minimales_Magnetfeld}
B_{min}=\dfrac{M(year)}{(r+LEO_{max})^3} * \sqrt{1+3*(sin(\lambda_{min}))^2}
\end{equation}
\begin{equation}\label{eq:Minimales_Magnetfeld_solved}
B_{min}=\dfrac{7,746*10^{24}*nT*m^3}{(6378155m+2000km)^3} * \sqrt{1+3*(sin(-90^{\circ}))^2} = 26,343\mu T
\end{equation}
In Gleichung \ref{eq:Minimales_Magnetfeld_solved} erhält man mit der maximalen Höhe im LEO und der minimalen geographischen beziehungsweise magnetischen Breite das minimale Erdmagnetfeld.
\begin{equation}\label{eq:Maximales_Magnetfeld_solved}
B_{max}=\dfrac{7,746*10^{24}*nT*m^3}{(6378155m+200km)^3} * \sqrt{1+3*(sin(90^{\circ}))^2} = 54,425\mu T
\end{equation}
In Gleichung \ref{eq:Maximales_Magnetfeld_solved} erhält man mit der minimalen Höhe im LEO und der maximalen geographischen beziehungsweise magnetischen Breite das maximale Erdmagnetfeld.
\\
Da in der Gleichung \ref{eq:Minimales_Magnetfeld} bei der geographischen beziehungsweise magnetischen Breite ein Quadrat vorkommt, spielt es keine Rolle, ob man hier die minimale oder maximale Breite nimmt.
\\
\\
Zur Veranschaulichung wurde das Erdmagnetfeld in Abhängigkeit vom LEO in Diagramm \ref{fig:Erdmagnetfeld - LEO} dargestellt.

\vspace{0.5cm}
\noindent\begin{minipage}{\linewidth}
	\centering
	\captionsetup{type=figure}
	\fbox{\includegraphics[height=0.65\textwidth]{05_media/01_images/F_ADCS/erdmagnetfeld_leo}}
	\captionof{figure}[Erdmagnetfeld - LEO]{Erdmagnetfeld - LEO}
	\label{fig:Erdmagnetfeld - LEO}
\end{minipage}
\vspace{0.3cm}

Es lässt sich erkennen, dass das Erdmagnetfeld mit zunehmender Höhe abnimmt.

\subsection{Mechanische Berechnungen}
\subsubsection{Trägheitsmoment}
Da das Trägheitsmoment bei einem Bewegungsablauf, vor allem beim Abbremsen im All, eine große Rolle spielt, muss dieses berechnet werden. Wir rechnen mit dem maximalen Gewicht von $1,33kg$ eines 1U-CubeSats mit dem Massemittelpunkt im Zentrum des Würfels.~\cite{bib:Homogener_Wuerfel}
Für die Seitenlänge wird mit $10cm$ gerechnet, was einem 1U-CubeSat entspricht.
\\
\\
Dies ist zwar eine Annäherungsrechnung, man hat jedoch kaum Abweichung, denn die CubeSat-Norm schreibt vor, dass der Massemittelpunkt in Nähe des Zentrums sein muss.~\cite{bib:CubeSat_Norm}
\begin{equation}\label{eq:Trägheitsmoment}
J_{SAT} = \dfrac{1}{6} * m_{SAT} * a_{SAT}^2
\end{equation}
\begin{equation}\label{eq:Trägheitsmoment_solved}
J_{SAT} = \dfrac{1}{6} * 1,33kg * 0.1m^2 = 0,002kg*m^2
\end{equation}

Wie man in Gleichung \ref{eq:Trägheitsmoment} erkennen kann, hängt das Verhalten des Trägheitsmoments vom Massemittelpunkt und der Masse ab.

\subsubsection{Winkelbeschleunigung}
\begin{equation}\label{eq:Winkelbeschleunigung_Grundgleichung}
\alpha_{SAT} = \dfrac{\varphi_{SAT}}{t_{max}^2}
\end{equation}

Gleichung \ref{eq:Winkelbeschleunigung_Grundgleichung} ist die Grundgleichung für die Winkelbeschleunigung.\\
Diese wird auf $t_{max}^2$ umgestellt.

\begin{equation}\label{eq:Winkelbeschleunigung_1}
t_{max} = \sqrt{\dfrac{\varphi_{SAT}}{\alpha_{SAT}}} = \sqrt{\dfrac{90^{\circ}}{\alpha_{SAT}}}
\end{equation}

Für $\varphi_{SAT}$ wird $90^{\circ}$ eingesetzt, da wir bei unseren Berechnungen von einer $90^{\circ}$-Umdrehung um eine Achse ausgehen.\\
\\
Da es im Weltall keinen Reibungswiderstand gibt, muss für die Beschleunigung genau so viel Energie wie für die Bremsung bis zum Stillstand aufgebracht werden.
Die Versorgungsleistung ist bei einem 1U-CubeSats stark limitiert, darum rechnen wir mit einem homogenen Weg-Zeit-Verlauf, um die maximal benötigte Leistung zu minimieren.\\
Es ergibt sich folgende Gleichung \ref{eq:Winkelbeschleunigung_2}.

\begin{equation}\label{eq:Winkelbeschleunigung_2}
t_{max} = \sqrt{\dfrac{45^{\circ}}{\alpha_{SAT}}} + \sqrt{\dfrac{45^{\circ}}{\alpha_{SAT}}} \rightarrow t_{max} = 2 * \sqrt{\dfrac{45^{\circ}}{\alpha_{SAT}}}
\end{equation}

Da wir jetzt jedoch die Winkelbeschleunigung $\alpha_{SAT}$ berechnen wollen, müssen wir die Gleichung wieder umstellen.

\begin{equation}\label{eq:Winkelbeschleunigung}
\alpha_{SAT} = \dfrac{180^{\circ}}{t_{max}^2} = \dfrac{180^{\circ}}{15min^2} = 2,2\overline{2}*10^{-4}rad*s^{-2}
\end{equation}

Für $t_{max}$ wird $15min$ eingesetzt, da der CubeSat die $90^{\circ}$-Umdrehung in diesem Zeitraum mit einer maximalen Stromzufuhr von $1A$ absolvieren soll.

\subsubsection{Moment}
Das benötigte mechanische Moment, das auf den Satelliten wirken soll, lässt sich nun über die Winkelbeschleunigung und dem Trägheitsmoment berechnen.

\begin{equation}\label{eq:Mechanisches_Moment}
M_{SAT} = \alpha_{SAT} * J_{SAT} = 2,2\overline{2}*10^{-4}rad*s^{-2} * 0,002kg*m^2
\end{equation}
\begin{equation}\label{eq:Mechanisches_Moment_solved}
M_{SAT} = 0,009 * 10^{-6}Nm
\end{equation}

Über das mechanische Moment und das minimale Erdmagnetfeld kann man mittels Gleichung \ref{eq:Magnetisches_Moment} das benötigte magnetische Moment, welches die Spule erzeugen muss, berechnen.

\begin{equation}\label{eq:Magnetisches_Moment}
m_{SAT} = \dfrac{M_{SAT}}{B_{min}} = \dfrac{0,009 * 10^{-6}Nm}{26,343\mu T} =3,264 * 10^{-4}A*m^2
\end{equation}

\clearpage
\subsection{Spule dimensionieren}
Als Spule wird eine Luftspule berechnet, da diese am einfachsten zu fertigen ist. Dazu wurde sehr viel getestet, bis letztendlich realistische Werte herauskamen.\\
\\
Für die Berechnung müssen zuerst einige Werte angenommen beziehungsweise bestimmt werden.

\subsubsection{Kupferlackdraht}
Als Draht wurde ein einfacher Kupferlackdraht gewählt.~\cite{bib:Spule}
\begin{equation}\label{eq:Drahtdurchmesser}
d_{CU} = 0,5mm
\end{equation}
Gleichung \ref{eq:Drahtdurchmesser} gibt den Drahtdurchmesser ohne Lack und Gleichung \ref{eq:Drahtdurchmesser_Lack} gibt den Drahtdurchmesser mit Lack an.
\begin{equation}\label{eq:Drahtdurchmesser_Lack}
D_{CU} = 0,6mm
\end{equation}

\begin{equation}\label{eq:Gleichstromwiderstand_Draht}
\Delta R_{DC} = 0,0871\frac{\Omega}{m}
\end{equation} 

Gleichung \ref{eq:Gleichstromwiderstand_Draht} gibt den Gleichstromwiderstand des Kupferlackdrahtes an.

\clearpage
\subsubsection{Abmessungen}

\vspace{0.5cm}
\noindent\begin{minipage}{\linewidth}
	\centering
	\captionsetup{type=figure}
	\includegraphics[height=0.65\textwidth]{05_media/01_images/F_ADCS/Spule_2}
	\captionof{figure}[Spule - Querschnitt]{Spule - Querschnitt}
	\label{fig:Spule - Querschnitt}
\end{minipage}

\begin{equation}\label{eq:Spule_Aussenlaenge}
l_{a} = 90mm
\end{equation}

Die Außenlänge \ref{eq:Spule_Aussenlaenge} der Spule ist durch die Größe des Satelliten und der Außenplatine begrenzt.

\begin{equation}\label{eq:Spule_Höhe}
h_{Spulemax} = 1cm
\end{equation}

\begin{equation}\label{eq:Spule_Innenradius}
r_{i} = 3mm
\end{equation}

Die maximale Höhe \ref{eq:Spule_Höhe} und der Innenradius \ref{eq:Spule_Innenradius} der Spule sind ausgewählte Werte. Hierbei wurde sehr viel getestet, bis letztendlich realistische Werte herauskamen.  
\\
\\
Eine weitere Angabe zur Berechnung der Spule:

\begin{equation}\label{eq:Magnetische_Feldkonstante}
\mu_{0} = 4 * \pi * 10^{-7} *\frac{Vs}{Am}
\end{equation}

Die magnetische Feldkonstante \ref{eq:Magnetische_Feldkonstante} ist ein fix vorgegebener Wert.

\clearpage

\begin{equation}\label{eq:Drahtquerschnitt}
A_{d} = (\frac{d_{CU}}{2}) * \pi = 1,963 * 10^{-7} m^2
\end{equation}

\begin{equation}\label{eq:Drahtquerschnitt_Lack}
A_{D} = (\frac{D_{CU}}{2}) * \pi = 2.827 * 10^{-7} m^2
\end{equation}

Gleichung \ref{eq:Drahtquerschnitt} gibt den Drahtquerschnitt ohne Lack und Gleichung \ref{eq:Drahtquerschnitt_Lack} gibt den Drahtquerschnitt mit Lack an.

\begin{equation}\label{eq:Spule_Windungen}
N = \frac{m_{SAT}}{2 * A_{d} * 1A} = 831,08
\end{equation}

Die Windungen der Spule~\cite{bib:Windungen} aus der Gleichung \ref{eq:Spule_Windungen} ergeben sich aus dem magnetischen Moment, dem Strom der durch die Spule fließt, aus dem Drahtquerschnitt ohne Lack und dem maximal möglichen Strom.

\vspace{0.7cm}
\noindent\begin{minipage}{\linewidth}
	\centering
	\captionsetup{type=figure}
	\includegraphics[height=0.25\textwidth]{05_media/01_images/F_ADCS/Spule_3}
	\captionof{figure}[Annäherung zur Berechnung des Querschnitts der Spule]{Annäherung zur Berechnung des Querschnitts der Spule}
	\label{fig:Annäherung zur Berechnung des Querschnitts der Spule}
\end{minipage}
\vspace{0.3cm}

In der Abbildung \ref{fig:Annäherung zur Berechnung des Querschnitts der Spule} sieht man die zur Berechnung des Querschnitts der Spule durchdachte Annäherung. Hiermit ergibt sich in Gleichung \ref{eq:Spule_Querschnitt} folgender Wert.  

\begin{equation}\label{eq:Spule_Querschnitt}
A_{Q} = N * d_{CU}^2 = 2,078 * 10^{-4} m^2
\end{equation}

Somit lassen sich dann die restlichen Abmessungen der Spule berechnen. Der Rechenweg lässt sich aus der Abbildung \ref{fig:Spule - Querschnitt} leicht herleiten.

\begin{equation}\label{eq:Spule_Breite}
b_{Spule} = \dfrac{A_{Q}}{h_{Spulemax}} = 20,777mm
\end{equation}

Aus der Breite der Spule \ref{eq:Spule_Breite} und dem Innenradius der Spule \ref{eq:Spule_Innenradius} lässt sich der mittlere Radius und daraus dann der mittlere Umfang der Spule berechnen.

\begin{equation}\label{eq:Spule_Mittlere_Radius}
r_{m} = r_{i} + \dfrac{b_{Spule}}{2} = 13,389mm
\end{equation}

\clearpage

\begin{equation}\label{eq:Spule_Mittlere_Umfang}
U_{m} = \dfrac{2 * r_{m} * \pi}{4} = 21,031mm
\end{equation}

\begin{equation}\label{eq:Spule_Innere_Länge}
l_{i} = l_{a} - 2 * b_{Spule} = 48,446mm
\end{equation}

Die innere Länge \ref{eq:Spule_Innere_Länge} berechnet sich aus der Subtraktion der Außenlänge mit der Breite der Spule.

\begin{equation}\label{eq:Spule_Mittlere_Feldlinienlänge}
l_{m} = 4 * (l_{i} - 2 * r_{i}) + U_{m} = 190,815mm
\end{equation}

Die Gleichung \ref{eq:Spule_Mittlere_Feldlinienlänge} gibt die mittlere Feldlinienlänge an. Aus dieser lässt sich dann die Länge des Leiters berechnen.

\begin{equation}\label{eq:Spule_Leiterlänge}
l_{Leiter} = l_{m} * N = 158,582m
\end{equation}

Zum Schluss wird noch der Leiterwiderstand und die Induktivität des Leiters berechnet.~\cite{bib:induktivitaet}

\begin{equation}\label{eq:Leiterwiderstand_Leiter}
R_{Leiter} = \Delta R_{DC} * l_{leiter} = 13,813\Omega
\end{equation}

\begin{equation}\label{eq:Induktivität_Leiter}
L_{m} = \mu_{0} * \dfrac{N^2 * A_{Q}}{l_{m}} = 0,948mH 
\end{equation}

