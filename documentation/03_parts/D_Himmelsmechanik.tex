\clearpage
\section{Himmelsmechanik}
Die Himmelsmechanik beschreibt die Bewegung astronomischer Objekte aufgrund physikalischer Theorien und Modelle.\\
\\
Man bezeichnet Flugkörper nur dann als Satelliten, wenn sie die Erde im Weltraum umkreisen. Alle Flugkörper, die den Erdorbit mit Fluchtgeschwindigkeit verlassen oder in eine Umlaufbahn um den Mond einschwenken, werden Raumsonden genannt, unabhängig davon, ob sie als Orbiter in den Orbit eines anderen Planeten eintreten oder nicht. Einem Satelliten fehlt auch nach Erreichen seiner Laufbahn ein Eigenantrieb, was ihn vom Raumschiff unterscheidet.~\cite{bib:CubeSat}
\\
\\
Beim Start eines Satelliten muss seine Bahngeschwindigkeit so hoch sein, dass seine Zentrifugalkraft mindestens gleich der Gewichtskraft ist.
\begin{quote}
\textit{In der Himmelsmechanik bezeichnet Bahngeschwindigkeit die Geschwindigkeit, mit der sich ein astronomisches Objekt bewegt.\\
Bei Umlaufbahnen spricht man auch von Orbitalgeschwindigkeit oder Umlaufgeschwindigkeit.}
~\cite{bib:Wikipedia_Bahngeschwindigkeit}
\end{quote}

\clearpage
\subsection{Kosmische Geschwindigkeiten}
Es gibt drei kosmische Geschwindigkeiten, wobei für Satelliten nur die erste und zweite von Bedeutung ist. ~\cite{bib:Kosmische_Geschwindigkeiten}
\subsubsection{Erste kosmische Geschwindigkeit}
\begin{equation} \label{eq:Erste_kosmische_Geschwindigkeit_Gleichung}
F_r = F_G \rightarrow \dfrac{m_1 * v_1^2}{r} = G * \dfrac{m_1 * m_2}{r^2}
\end{equation}
\begin{equation} \label{eq:Erste_kosmische_Geschwindigkeit}
v_1 = \sqrt[2]{G * \dfrac{m_2}{r}} = 7,9\dfrac{km}{s}
\end{equation}
Für eine erdnahe, kreisförmige Umlaufbahn gilt die Erste kosmische Geschwindigkeit aus Gleichung~\ref{eq:Erste_kosmische_Geschwindigkeit}.\\
\\
Bei einem Start in Ostrichtung trägt die Erddrehung mit einem Anteil von maximal $0,46 km/s$ am Äquator zur Bahngeschwindigkeit bei. Jedoch kommt es nicht zu einem vollständigen Ausnutzen der Erdrotation, da der Flugkörper aufgrund von sich in andere Richtungen bewegenden Luftteilchen (Winde) abgebremst wird. Für eine Rakete kann somit eine relative Geschwindigkeit von $7,44 km/s$ genügen um $v_1$ zu erreichen. In Westrichtung wäre der Anteil zusätzlich aufzubringen, deshalb werden fast alle Satelliten in Ostrichtung gestartet. Die Kreisbahngeschwindigkeit polarer Bahnen bleibt von der Erdrotation unbeeinflusst.\\
\\
Die Gravitationskraft der Erde wirkt in diesem Fall als Zentripetalkraft, welche den Flugkörper auf eine Kreisbahn zwingt.

\subsubsection{Zweite kosmische Geschwindigkeit}
\begin{equation} \label{eq:Zweite_kosmische_Geschwindigkeit}
E_{kin} = \Delta W \rightarrow v_1 = \sqrt[2]{2 * G * \dfrac{m_2}{r}} = 11,2\dfrac{km}{s}
\end{equation}
Will man das Gravitationsfeld der Erde verlassen, muss der Satellit auf die zweite kosmische Geschwindigkeit von etwa $11,2 km/s$ beschleunigt werden. Sie entspricht dem $\sqrt[2]{2}$ -fachen der ersten kosmischen Geschwindigkeit.

\clearpage
\subsection{Satellitenbahnen}
Die antriebslose Bewegung eines Satelliten gehorcht genähert den Gesetzen des Zweikörperproblems der Himmelsmechanik – weitere Kräfte bewirken jedoch Bahnstörungen. Wäre die Erde eine exakte Kugel ohne Erdatmosphäre und gäbe es keine anderen Himmelskörper, folgte die Satellitenbahn einer mehr oder weniger exzentrischen Ellipse um die Erde gemäß den Keplerschen Gesetzen. Die Bahnebenen der Erdsatelliten gehen durch den Erdmittelpunkt und sind näherungsweise raumfest, also gegenüber den Fixsternen unverändert, während die Erde darunter rotiert.\\
\\
Abhängig von ihrer Flughöhe werden Satelliten in verschiedene Typen aufgeteilt:
~\cite{bib:Satellitenorbit}

\begin{itemize}
	\item GEO (Geostationary Orbit): geostationäre Satelliten mit einer Flughöhe von etwa $35.790km$. Hier beträgt die Umlaufzeit genau einen Tag. In Bezug auf die Erdoberfläche sind diese Satelliten ortsfest. Beispiele: Astra, Eutelsat, Inmarsat, Meteosat, etc.
	\item MEO (Medium Earth Orbit): Satelliten mit einer Flughöhe von $6.000-36.000km$ und einer Umlaufdauer von 4 – 24 Stunden. Beispiele: GPS, GLONASS, Galileo, etc.
	\item LEO (Low Earth Orbit): Satelliten mit einer Flughöhe von $200-2.000km$ und einer Umlaufdauer von 1,5 – 2 Stunden. Beispiele: Iridium, Globalstar, GLAST, etc.
\end{itemize}

Die Erdatmosphäre bewirkt eine ständige leichte Bremsung der Satelliten, sodass sich Bahnen unter einer Höhe von etwa $1.000km$ spiralförmig der Erde nähern. Die Lebensdauer hängt auch vom Verhältnis Oberfläche/Masse ab und reicht von einigen Wochen oder Jahren (LEOs) bis zu Jahrtausenden (MEOs). Weitere Bahnstörungen werden von der Gravitation des Mondes verursacht, vom Strahlungsdruck der Sonne und von Effekten in der Ionosphäre. Die Satellitenbahn muss deshalb ständig kontrolliert und gegebenenfalls nachgeregelt werden (Attitude Determination and Control System). Wenn der Gasvorrat für die Korrekturdüsen aufgebraucht ist, verlässt der Satellit seine Umlaufbahn und wird dadurch meist wertlos.

\clearpage
\subsubsection{Low Earth Orbit}
Der Low Earth Orbit (LEO, niedrige Erdumlaufbahn, erdnahe Umlaufbahn) liegt in einer Höhe von $200-2000km$. Sie ist mitunter die energieärmste und zugleich die mit Abstand am leichtesten erreichbare Umlaufbahn. Die Geschwindigkeit der Raumfahrzeuge kann in Abhängigkeit der Höhe unterschiedlich sein, wobei sie im LEO ungefähr $7km/s$ beträgt. Die Zeit, die die Raumfahrzeuge für einen Umlauf um die Erde benötigen, beträgt ca. 100 Minuten, wobei sie von der Geschwindigkeit und respektive von der Höhe des Raumfahrzeuges im LEO abhängt.\\
\\
CubeSats werden hauptsächlich im LEO freigesetzt, darum beziehen sich alle nachfolgenden Berechnungen ausschließlich auf diese. Ein Grund dafür ist, dass bei steigender Erdentfernung die Anforderungen an die Satelliten immer höher werden und diese mit einem CubeSat kaum eingehalten werden können.\\
\\
Der LEO wird außerdem auch für Amateurfunksatelliten genutzt, weshalb sie auch für uns eine wesentliche Hauptrolle spielt.~\cite{bib:LEO}
