LESEN:
0x3D

SCHREIBEN:
0x3C

BEFEHLE (ASCII):
LESEN:

S3C 03 R 01 P -> MSB von X-Achse auslesen
S3C 04 R 01 P -> LSB von X-Achse auslesen
S3C 03 R 01 S3C 04 R 01 P -> MSB + LSB auslesen

S3C 07 R 01 P -> MSB von Y-Achse auslesen
S3C 08 R 01 P -> LSB von Y-Achse auslesen

S3C 05 R 01 P -> MSB von Z-Achse auslesen
S3C 06 R 01 P -> LSB von Z-Achse auslesen

S3C 00 R 01 P -> Configuration Register A auslesen
S3C 01 R 01 P -> Configuration Register B auslesen
S3C 02 R 01 P -> Mode Register auslesen

SCHREIBEN:
("??" steht f�r den Datenwert in HEX welche in das Register geschrieben werden soll)

S3C 00 ?? P -> Configuration Register A schreiben
S3C 01 ?? P -> Configuration Register B schreiben
S3C 02 ?? P -> Mode Register schreiben