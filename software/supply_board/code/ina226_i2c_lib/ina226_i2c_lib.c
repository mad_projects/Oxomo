/******************************************************************************

ina226 i2c libary

@date 01.02.2017
@author Mithrandir

*******************************************************************************/

#include <avr/io.h>
#include <stdint.h>

#include "ina226_i2c_lib.h"
#include "i2cmaster.h"

#define i2c_dev_addr 0x00

uint16_t ina226_configuration(uint8_t byte1 uint8_t byte2 uint8_t mode) { // R/W operation

	unsigned char ret;
	uint8_t register_pointer = 0x00;

	if (mode == 0) { // read mode

		// first set the correct pointer value

		ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	    if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	        /* failed to issue start condition, possibly no device found */
	        i2c_stop();

	    }

	    else {

	        /* issuing start condition ok, device accessible */
	        i2c_write(register_pointer);                      
	        i2c_stop();                            // set stop conditon = release bus

	    }

	    // then read the data

		i2c_start_wait(i2c_dev_addr + I2C_READ);       // set device address and read mode
		byte1 = i2c_readAck(); // read first byte, follow with ack
		byte2 = i2c_readNak(); // read secound byte, mark the end of the transmission
		i2c_stop();

		uint16_t read_value = (byte1 << 8) | byte2; // just seperate again with 'byte1 = read_value & 0xFF;' and 'byte2 = (read_value >> 8) & 0xFF;'

		return read_value;

	}

	else { // write mode

		ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	    if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	        /* failed to issue start condition, possibly no device found */
	        i2c_stop();

	        return 1; // error

	    }

	    else {

	        /* issuing start condition ok, device accessible */
	        i2c_write(register_pointer);
	        i2c_write(byte1);
	        i2c_write(byte2);                       
	        i2c_stop();                            // set stop conditon = release bus

	        return 0; // no error
	    
	    }

	}
	
}

uint16_t ina226_shunt_voltage() { // R operation

	unsigned char ret;
	uint8_t register_pointer = 0x01;
	uint8_t byte1;
	uint8_t byte2;

	// first set the correct pointer value

	ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	    /* failed to issue start condition, possibly no device found */
	    i2c_stop();

	}

	else {

	    /* issuing start condition ok, device accessible */
	    i2c_write(register_pointer);                      
	    i2c_stop();                            // set stop conditon = release bus

	}

	// then read the data

	i2c_start_wait(i2c_dev_addr + I2C_READ);       // set device address and write mode
	byte1 = i2c_readAck(); // read first byte, follow with ack
	byte2 = i2c_readNak(); // read secound byte, mark the end of the transmission
	i2c_stop();

	uint16_t read_value = (byte1 << 8) | byte2; // just seperate again with 'byte1 = read_value & 0xFF;' and 'byte2 = (read_value >> 8) & 0xFF;'

	return read_value;

}

uint16_t ina226_bus_voltage() { // R operation

	unsigned char ret;
	uint8_t register_pointer = 0x02;
	uint8_t byte1;
	uint8_t byte2;

	// first set the correct pointer value

	ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	    /* failed to issue start condition, possibly no device found */
	    i2c_stop();

	}

	else {

	    /* issuing start condition ok, device accessible */
	    i2c_write(register_pointer);                      
	    i2c_stop();                            // set stop conditon = release bus

	}

	// then read the data

	i2c_start_wait(i2c_dev_addr + I2C_READ);       // set device address and write mode
	byte1 = i2c_readAck(); // read first byte, follow with ack
	byte2 = i2c_readNak(); // read secound byte, mark the end of the transmission
	i2c_stop();

	uint16_t read_value = (byte1 << 8) | byte2; // just seperate again with 'byte1 = read_value & 0xFF;' and 'byte2 = (read_value >> 8) & 0xFF;'

	return read_value;

}

uint16_t ina226_power() { // R operation

	unsigned char ret;
	uint8_t register_pointer = 0x03;
	uint8_t byte1;
	uint8_t byte2;

	// first set the correct pointer value

	ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	    /* failed to issue start condition, possibly no device found */
	    i2c_stop();

	}

	else {

	    /* issuing start condition ok, device accessible */
	    i2c_write(register_pointer);                      
	    i2c_stop();                            // set stop conditon = release bus

	}

	// then read the data

	i2c_start_wait(i2c_dev_addr + I2C_READ);       // set device address and write mode
	byte1 = i2c_readAck(); // read first byte, follow with ack
	byte2 = i2c_readNak(); // read secound byte, mark the end of the transmission
	i2c_stop();

	uint16_t read_value = (byte1 << 8) | byte2; // just seperate again with 'byte1 = read_value & 0xFF;' and 'byte2 = (read_value >> 8) & 0xFF;'

	return read_value;

}

uint16_t ina226_current() { // R operation

	unsigned char ret;
	uint8_t register_pointer = 0x04;
	uint8_t byte1;
	uint8_t byte2;

	// first set the correct pointer value

	ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	    /* failed to issue start condition, possibly no device found */
	    i2c_stop();

	}

	else {

	    /* issuing start condition ok, device accessible */
	    i2c_write(register_pointer);                      
	    i2c_stop();                            // set stop conditon = release bus

	}

	// then read the data

	i2c_start_wait(i2c_dev_addr + I2C_READ);       // set device address and write mode
	byte1 = i2c_readAck(); // read first byte, follow with ack
	byte2 = i2c_readNak(); // read secound byte, mark the end of the transmission
	i2c_stop();

	uint16_t read_value = (byte1 << 8) | byte2; // just seperate again with 'byte1 = read_value & 0xFF;' and 'byte2 = (read_value >> 8) & 0xFF;'

	return read_value;

}

uint16_t ina226_calibration(uint8_t byte1 uint8_t byte2 uint8_t mode) { // R/W operation

	unsigned char ret;
	uint8_t register_pointer = 0x05;

	if (mode == 0) { // read mode

		// first set the correct pointer value

		ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	    if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	        /* failed to issue start condition, possibly no device found */
	        i2c_stop();

	    }

	    else {

	        /* issuing start condition ok, device accessible */
	        i2c_write(register_pointer);                      
	        i2c_stop();                            // set stop conditon = release bus

	    }

	    // then read the data

		i2c_start_wait(i2c_dev_addr + I2C_READ);       // set device address and read mode
		byte1 = i2c_readAck(); // read first byte, follow with ack
		byte2 = i2c_readNak(); // read secound byte, mark the end of the transmission
		i2c_stop();

		uint16_t read_value = (byte1 << 8) | byte2; // just seperate again with 'byte1 = read_value & 0xFF;' and 'byte2 = (read_value >> 8) & 0xFF;'

		return read_value;

	}

	else { // write mode

		ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	    if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	        /* failed to issue start condition, possibly no device found */
	        i2c_stop();

	        return 1; // error

	    }

	    else {

	        /* issuing start condition ok, device accessible */
	        i2c_write(register_pointer);
	        i2c_write(byte1);
	        i2c_write(byte2);                       
	        i2c_stop();                            // set stop conditon = release bus

	        return 0; // no error
	    
	    }

	}
	
}

uint16_t ina226_mask_enable(uint8_t byte1 uint8_t byte2 uint8_t mode) { // R/W operation

	unsigned char ret;
	uint8_t register_pointer = 0x06;

	if (mode == 0) { // read mode

		// first set the correct pointer value

		ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	    if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	        /* failed to issue start condition, possibly no device found */
	        i2c_stop();

	    }

	    else {

	        /* issuing start condition ok, device accessible */
	        i2c_write(register_pointer);                      
	        i2c_stop();                            // set stop conditon = release bus

	    }

	    // then read the data

		i2c_start_wait(i2c_dev_addr + I2C_READ);       // set device address and read mode
		byte1 = i2c_readAck(); // read first byte, follow with ack
		byte2 = i2c_readNak(); // read secound byte, mark the end of the transmission
		i2c_stop();

		uint16_t read_value = (byte1 << 8) | byte2; // just seperate again with 'byte1 = read_value & 0xFF;' and 'byte2 = (read_value >> 8) & 0xFF;'

		return read_value;

	}

	else { // write mode

		ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	    if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	        /* failed to issue start condition, possibly no device found */
	        i2c_stop();

	        return 1; // error

	    }

	    else {

	        /* issuing start condition ok, device accessible */
	        i2c_write(register_pointer);
	        i2c_write(byte1);
	        i2c_write(byte2);                       
	        i2c_stop();                            // set stop conditon = release bus

	        return 0; // no error
	    
	    }

	}
	
}

uint16_t ina226_alert_limit(uint8_t byte1 uint8_t byte2 uint8_t mode) { // R/W operation

	unsigned char ret;
	uint8_t register_pointer = 0x07;

	if (mode == 0) { // read mode

		// first set the correct pointer value

		ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	    if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	        /* failed to issue start condition, possibly no device found */
	        i2c_stop();

	    }

	    else {

	        /* issuing start condition ok, device accessible */
	        i2c_write(register_pointer);                      
	        i2c_stop();                            // set stop conditon = release bus

	    }

	    // then read the data

		i2c_start_wait(i2c_dev_addr + I2C_READ);       // set device address and read mode
		byte1 = i2c_readAck(); // read first byte, follow with ack
		byte2 = i2c_readNak(); // read secound byte, mark the end of the transmission
		i2c_stop();

		uint16_t read_value = (byte1 << 8) | byte2; // just seperate again with 'byte1 = read_value & 0xFF;' and 'byte2 = (read_value >> 8) & 0xFF;'

		return read_value;

	}

	else { // write mode

		ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	    if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	        /* failed to issue start condition, possibly no device found */
	        i2c_stop();

	        return 1; // error

	    }

	    else {

	        /* issuing start condition ok, device accessible */
	        i2c_write(register_pointer);
	        i2c_write(byte1);
	        i2c_write(byte2);                       
	        i2c_stop();                            // set stop conditon = release bus

	        return 0; // no error
	    
	    }

	}
	
}

uint16_t ina226_manufacturer_id() { // R operation

	unsigned char ret;
	uint8_t register_pointer = 0xFE;
	uint8_t byte1;
	uint8_t byte2;

	// first set the correct pointer value

	ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	    /* failed to issue start condition, possibly no device found */
	    i2c_stop();

	}

	else {

	    /* issuing start condition ok, device accessible */
	    i2c_write(register_pointer);                      
	    i2c_stop();                            // set stop conditon = release bus

	}

	// then read the data

	i2c_start_wait(i2c_dev_addr + I2C_READ);       // set device address and write mode
	byte1 = i2c_readAck(); // read first byte, follow with ack
	byte2 = i2c_readNak(); // read secound byte, mark the end of the transmission
	i2c_stop();

	uint16_t read_value = (byte1 << 8) | byte2; // just seperate again with 'byte1 = read_value & 0xFF;' and 'byte2 = (read_value >> 8) & 0xFF;'

	return read_value;

}

uint16_t ina226_die_id() { // R operation

	unsigned char ret;
	uint8_t register_pointer = 0xFF;
	uint8_t byte1;
	uint8_t byte2;

	// first set the correct pointer value

	ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	    /* failed to issue start condition, possibly no device found */
	    i2c_stop();

	}

	else {

	    /* issuing start condition ok, device accessible */
	    i2c_write(register_pointer);                      
	    i2c_stop();                            // set stop conditon = release bus

	}

	// then read the data

	i2c_start_wait(i2c_dev_addr + I2C_READ);       // set device address and write mode
	byte1 = i2c_readAck(); // read first byte, follow with ack
	byte2 = i2c_readNak(); // read secound byte, mark the end of the transmission
	i2c_stop();

	uint16_t read_value = (byte1 << 8) | byte2; // just seperate again with 'byte1 = read_value & 0xFF;' and 'byte2 = (read_value >> 8) & 0xFF;'

	return read_value;

}