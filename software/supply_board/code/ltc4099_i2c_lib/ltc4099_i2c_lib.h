/******************************************************************************

ltc4099_ i2c libary

	information about write data:

		All the information can be found in the datasheet page 23, table 1.

	information about read data:

		All the information can be found in the datasheet page 24, table 6.

@date 01.02.2017
@author Mithrandir

*******************************************************************************/

#include <avr/io.h>
#include <stdint.h>

#ifndef LTC4099_I2C_LIB_H_INCLUDED
#define LTC4099_I2C_LIB_H_INCLUDED

extern uint8_t ltc4099_command_0(uint8_t byte1) { // W operation
extern uint8_t ltc4099_command_1(uint8_t byte1) { // W operation
extern uint8_t ltc4099_irq_mask(uint8_t byte1) { // W operation
extern uint8_t ltc4099_status_data() { // R operation

#endif