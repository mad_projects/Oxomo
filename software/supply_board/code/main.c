/******************************************************************************

main

@date 01.02.2017
@author Mithrandir

*******************************************************************************/

#include <avr/io.h>
#include <stdint.h>

#include "ina226_i2c_lib.h"
#include "ltc4099_i2c_lib.h"
#include "max7323_i2c_lib.h"

int main(void) {

    i2c_init();                                // init I2C interface

    // do stuff

} // int main (void)