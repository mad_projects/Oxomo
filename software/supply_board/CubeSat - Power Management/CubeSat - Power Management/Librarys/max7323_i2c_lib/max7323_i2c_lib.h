/******************************************************************************

max7323 i2c libary

	byte1 = most significant byte
	byte2 = least significant byte

	information about write data:

		A 2-byte write to the MAX7323, the first byte sets
		the logic state of the four open-drain I/O ports and four
		push-pull outputs, while the second byte sets the interrupt
		mask bits for the four open-drain I/O ports.

	information about read data:

		A 2-byte read returns first the status of the four I/O ports
		and the four output ports (as for a single-byte read),
		followed by the four transition flags for the four I/O ports.

@date 01.02.2017
@author Mithrandir

*******************************************************************************/

#include <avr/io.h>
#include <stdint.h>

#ifndef MAX7323_I2C_LIB_H_INCLUDED
#define MAX7323_I2C_LIB_H_INCLUDED

extern uint8_t max7323_write(uint8_t byte1 uint8_t byte2) { // W operation
extern uint16_t max7323_read() { // R operation

#endif