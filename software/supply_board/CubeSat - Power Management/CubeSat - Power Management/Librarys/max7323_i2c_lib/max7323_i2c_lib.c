/******************************************************************************

max7323 i2c libary

@date 01.02.2017
@author Mithrandir

*******************************************************************************/

#include <avr/io.h>
#include <stdint.h>

#include "max7323_i2c_lib.h"
#include "i2cmaster.h"

#define i2c_dev_addr 0x12

uint8_t max7323_write(uint8_t byte1 uint8_t byte2) { // W operation

	unsigned char ret;

	ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	    /* failed to issue start condition, possibly no device found */
	    i2c_stop();

	    return 1; // error

	}

	else {

	    /* issuing start condition ok, device accessible */
	    i2c_write(byte1);
	    i2c_write(byte2);
	    i2c_stop();                            // set stop conditon = release bus

	    return 0; // no error

	}
	
}

uint16_t max7323_read() { // R operation

	unsigned char ret;
	uint8_t byte1;
	uint8_t byte2;

	ret = i2c_start(i2c_dev_addr + I2C_READ);       // set device address and read mode

	if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	    /* failed to issue start condition, possibly no device found */
	    i2c_stop();

	}

	else {

	    /* issuing start condition ok, device accessible */
	    byte1 = i2c_readAck(); // read first byte, follow with ack
	    byte2 = i2c_readNak(); // read byte, mark the end of the transmission
		i2c_stop();                           // set stop conditon = release bus

	}

	uint16_t read_value = (byte1 << 8) | byte2; // just seperate again with 'byte1 = read_value & 0xFF;' and 'byte2 = (read_value >> 8) & 0xFF;'

	return read_value;

}