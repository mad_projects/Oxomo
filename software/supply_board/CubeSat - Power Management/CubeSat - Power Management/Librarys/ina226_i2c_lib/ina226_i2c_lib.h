/******************************************************************************

ina226 i2c libary

	byte1 = most significant byte
	byte2 = least significant byte
	mode = 1 selects write mode, 0 selects read mode

	information about write/read data:

		All the information can be found in the datasheet page 21, table 4.

@date 01.02.2017
@author Mithrandir

*******************************************************************************/

#include <avr/io.h>
#include <stdint.h>

#ifndef INA226_I2C_LIB_H_INCLUDED
#define INA226_I2C_LIB_H_INCLUDED

extern uint16_t ina226_configuration(uint8_t byte1 uint8_t byte2 uint8_t mode) { // R/W operation
extern uint16_t ina226_shunt_voltage() { // R operation
extern uint16_t ina226_bus_voltage() { // R operation
extern uint16_t ina226_power() { // R operation
extern uint16_t ina226_current() { // R operation
extern uint16_t ina226_calibration(uint8_t byte1 uint8_t byte2 uint8_t mode) { // R/W operation
extern uint16_t ina226_mask_enable(uint8_t byte1 uint8_t byte2 uint8_t mode) { // R/W operation
extern uint16_t ina226_alert_limit(uint8_t byte1 uint8_t byte2 uint8_t mode) { // R/W operation
extern uint16_t ina226_manufacturer_id() { // R operation
extern uint16_t ina226_die_id() { // R operation

#endif