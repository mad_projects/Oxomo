/******************************************************************************

ltc4099_ i2c libary

@date 01.02.2017
@author Mithrandir

*******************************************************************************/

#include <avr/io.h>
#include <stdint.h>

#include "ltc4099_i2c_lib.h"
#include "i2cmaster.h"

#define i2c_dev_addr 0x12

uint8_t ltc4099_command_0(uint8_t byte1) { // W operation

	unsigned char ret;
	uint8_t register_pointer = 0x00;

	ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	    /* failed to issue start condition, possibly no device found */
	    i2c_stop();

	    return 1; // error

	}

	else {

	    /* issuing start condition ok, device accessible */
	    i2c_write(register_pointer);
	    i2c_write(byte1);                     
	    i2c_stop();                            // set stop conditon = release bus

	    return 0; // no error

	}
	
}

uint8_t ltc4099_command_1(uint8_t byte1) { // W operation

	unsigned char ret;
	uint8_t register_pointer = 0x01;

	ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	    /* failed to issue start condition, possibly no device found */
	    i2c_stop();

	    return 1; // error

	}

	else {

	    /* issuing start condition ok, device accessible */
	    i2c_write(register_pointer);
	    i2c_write(byte1);                     
	    i2c_stop();                            // set stop conditon = release bus

	    return 0; // no error

	}
	
}

uint8_t ltc4099_irq_mask(uint8_t byte1) { // W operation

	unsigned char ret;
	uint8_t register_pointer = 0x02;

	ret = i2c_start(i2c_dev_addr + I2C_WRITE);       // set device address and write mode

	if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	    /* failed to issue start condition, possibly no device found */
	    i2c_stop();

	    return 1; // error

	}

	else {

	    /* issuing start condition ok, device accessible */
	    i2c_write(register_pointer);
	    i2c_write(byte1);                     
	    i2c_stop();                            // set stop conditon = release bus

	    return 0; // no error

	}
	
}

uint8_t ltc4099_status_data() { // R operation

	unsigned char ret;
	uint8_t byte1;

	ret = i2c_start(i2c_dev_addr + I2C_READ);       // set device address and read mode

	if (ret) { // ret=0 -> Ok, ret=1 -> no ACK 

	    /* failed to issue start condition, possibly no device found */
	    i2c_stop();

	}

	else {

	    /* issuing start condition ok, device accessible */
	    byte1 = i2c_readNak(); // read byte, mark the end of the transmission
		i2c_stop();                           // set stop conditon = release bus

	}

	return byte1;

}